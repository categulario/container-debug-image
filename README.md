# Container debug image

Just a container image that I plug volumes into to debug them. It contains the
following sofware:

* neovim
* bat
* ss
* curl
* exa
* dust
* ripgrep
* [fd](https://github.com/sharkdp/fd)
* fish shell
* sqlite3

## Usage

Simply plug a volume or put the container in the same network as other
containers you want to debug.

    podman run -it --rm registry.gitlab.com/categulario/debugimage
